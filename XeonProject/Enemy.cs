﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;                                
using Microsoft.Xna.Framework.Media;
using Microsoft.Xna.Framework.Net;
using Microsoft.Xna.Framework.Storage;

using FarseerGames.FarseerPhysics;
using FarseerGames.FarseerPhysics.Dynamics;
using FarseerGames.FarseerPhysics.Dynamics.Joints;
using FarseerGames.FarseerPhysics.Collisions;
using FarseerGames.FarseerPhysics.Factories;

namespace XeonProject
{
    class Enemy
    {
        Body enemyBody;
        Geom enemyGeom;
        Texture2D enemyTexture;
        Vector2 enemyOrigin;
        string type;
        public int life;
        bool isDead;
        bool checkedDestroyed;

        Player chosenPlayer;
        bool playerNotChosen = true;

        private static Random random = new Random();
        private static Random random2 = new Random();
        private static Random random3 = new Random();
        private static Random random4 = new Random();
        Player selPlayer;

        int randomX;
        int randomY;

        public static int numberOfEnnemies = 8;

        public Enemy(Texture2D loadedtexture, string whatType)
        {
            enemyTexture = loadedtexture;
            enemyOrigin = new Vector2(this.enemyTexture.Width / 2, this.enemyTexture.Height / 2);
            type = whatType;

            if (type == "Tornado")
            {
                enemyBody = BodyFactory.Instance.CreateCircleBody(this.enemyTexture.Width / 2, 1);
                enemyGeom = GeomFactory.Instance.CreateCircleGeom(this.enemyBody, 22, 50);
                life = 5;
            }
            if (type == "Scouter")
            {
                enemyBody = BodyFactory.Instance.CreateCircleBody(this.enemyTexture.Width / 2, 1);
                enemyGeom = GeomFactory.Instance.CreateCircleGeom(this.enemyBody, 10, 50);
                life = 1;
            }
            if (type == "Bouncer")
            {
                enemyBody = BodyFactory.Instance.CreateRectangleBody(this.enemyTexture.Height, this.enemyTexture.Width, 1);
                enemyGeom = GeomFactory.Instance.CreateRectangleGeom(this.enemyBody, 50, 50);
                life = 1;
            }
            if (type == "Bouncer2")
            {
                enemyBody = BodyFactory.Instance.CreateRectangleBody(this.enemyTexture.Height, this.enemyTexture.Width, 1);
                enemyGeom = GeomFactory.Instance.CreateRectangleGeom(this.enemyBody, 50, 50);
                life = 1;
                this.setRotation(90);
            }
            if (type == "Boss")
            {
                enemyBody = BodyFactory.Instance.CreateCircleBody(this.enemyTexture.Width / 2, 1);
                enemyGeom = GeomFactory.Instance.CreateCircleGeom(this.enemyBody, 22, 50);
                life = 1;
            }
            if (type == "BossFire")
            {
                enemyBody = BodyFactory.Instance.CreateCircleBody(this.enemyTexture.Width / 2, 1);
                enemyGeom = GeomFactory.Instance.CreateCircleGeom(this.enemyBody, 22, 50);
                life = 1;
            }
            else
            {
                enemyBody = BodyFactory.Instance.CreateRectangleBody(this.enemyTexture.Height, this.enemyTexture.Width, 1);
                enemyGeom = GeomFactory.Instance.CreateRectangleGeom(this.enemyBody, 50, 50);
            }

            isDead = false;
            
            enemyBody.LinearDragCoefficient = 0.1F;
            enemyBody.RotationalDragCoefficient = 0.1F;

            enemyGeom.RestitutionCoefficient = 1.0f;
            enemyGeom.FrictionCoefficient = 0.0f;
            enemyGeom.CollisionCategories = CollisionCategory.Cat7;
            // On enleve le collide de 1 a 4 pour que ca soit la class Player qui decide quand Collide ou pas.
            enemyGeom.CollidesWith = CollisionCategory.All & ~CollisionCategory.Cat1 & ~CollisionCategory.Cat2 & ~CollisionCategory.Cat3 & ~CollisionCategory.Cat4 & ~CollisionCategory.Cat5 & ~CollisionCategory.Cat7;
            enemyGeom.Tag = this;
        }
        
        // ##### INIT #####

        public int findX(int min, int max, int unitNumber)
        {
            if (unitNumber % 2 == 0)
            {
                if (unitNumber % 4 == 0)
                randomX = this.randomX = random4.Next(min, max);
                else
                randomX = this.randomX = random.Next(min, max);
            }
            else if(unitNumber % 3 == 0)
                randomX = this.randomX = random2.Next(min, max);
            else if (unitNumber % 5 == 0)
                randomX = this.randomX = random3.Next(min, max);
            
            return randomX;
        }

        public int findY(int min, int max, int unitNumber)
        {
            if (unitNumber % 2 == 0)
            {
                if (unitNumber % 4 == 0)
                    randomY = this.randomY = random4.Next(min, max);
                else
                    randomY = this.randomY = random.Next(min, max);
            }
            else if (unitNumber % 3 == 0)
                randomY = this.randomY = random2.Next(min, max);
            else if (unitNumber % 5 == 0)
                randomY = this.randomY = random3.Next(min, max);

            return randomY;
        }


        public void init(int waveNumber, int unitNumber, PhysicsSimulator simulator)
        {
            switch (waveNumber)
                {
                case 1:
                        if( unitNumber == 0)
                            this.enemyBody.Position = new Vector2(70, 70);
                        else if(unitNumber == 1)
                            this.enemyBody.Position = new Vector2(640, 70);
                        else if(unitNumber == 2)
                            this.enemyBody.Position = new Vector2(1210, 70);
                        else if(unitNumber == 3)
                            this.enemyBody.Position = new Vector2(1210, 360);
                        else if(unitNumber == 4)
                            this.enemyBody.Position = new Vector2(1210, 650);
                        else if(unitNumber == 5)
                            this.enemyBody.Position = new Vector2(640, 650);
                        else if(unitNumber == 6)
                            this.enemyBody.Position = new Vector2(70, 650);
                        else if(unitNumber == 7)
                            this.enemyBody.Position = new Vector2(70, 360);
                        else
                            this.enemyBody.Position = new Vector2(70, 70);
                    break;
                case 2:
                    
                    randomX = findX(1150, 1200, unitNumber);
                    randomY = findY(80, 150, unitNumber);

                    this.enemyBody.Position = new Vector2(randomX, randomY);
                
                    break;
                case 3:

                    randomX = findX(1150, 1200, unitNumber);
                    randomY = findY(570, 640, unitNumber);

                    this.enemyBody.Position = new Vector2(randomX, randomY);
                    break;
                case 4:

                    randomX = findX(80, 150, unitNumber);
                    randomY = findY(570, 640, unitNumber);

                    this.enemyBody.Position = new Vector2(randomX, randomY);
                    break;
                case 5:

                    randomX = findX(80, 150, unitNumber);
                    randomY = findY(80, 150, unitNumber);

                    this.enemyBody.Position = new Vector2(randomX, randomY);
                    break;
                case 6:
                    switch (unitNumber)
                    {
                        case 0:
                            this.enemyBody.Position = new Vector2(70, 70);
                            break;
                        case 1:
                            this.enemyBody.Position = new Vector2(1210, 70);
                            break;
                        case 2:
                            this.enemyBody.Position = new Vector2(1210, 360);
                            break;
                        case 3:
                            this.enemyBody.Position = new Vector2(1210, 650);
                            break;
                        case 4:
                            this.enemyBody.Position = new Vector2(70, 650);
                            break;
                        case 5:
                            this.enemyBody.Position = new Vector2(70, 360);
                            break;
                        default:
                            this.enemyBody.Position = new Vector2(70, 70);
                            break;
                    }

                    break;
                case 7:
                    switch (unitNumber)
                    {
                        case 0:
                            this.enemyBody.Position = new Vector2(70, 70);
                            break;
                        case 1:
                            this.enemyBody.Position = new Vector2(1210, 70);
                            break;
                        case 2:
                            this.enemyBody.Position = new Vector2(1210, 70);
                            break;
                        case 3:
                            this.enemyBody.Position = new Vector2(1210, 650);
                            break;
                        case 4:
                            this.enemyBody.Position = new Vector2(70, 650);
                            break;
                        case 5:
                            this.enemyBody.Position = new Vector2(70, 650);
                            break;
                        default:
                            this.enemyBody.Position = new Vector2(70, 70);
                            break;
                    }
                    break;
                case 8:

                    randomX = findX(80, 1200, unitNumber);
                    randomY = findY(80, 150, unitNumber);

                    this.enemyBody.Position = new Vector2(randomX, randomY);
                    break;
                case 9:

                    randomX = findX(80, 1200, unitNumber);
                    randomY = findY(570, 640, unitNumber);

                    this.enemyBody.Position = new Vector2(randomX, randomY);
                    break;
                case 10:

                    randomX = findX(80, 150, unitNumber);
                    randomY = findY(80, 640, unitNumber);

                    this.enemyBody.Position = new Vector2(randomX, randomY);
                    break;
                case 11:

                    randomX = findX(1120, 1200, unitNumber);
                    randomY = findY(80, 640, unitNumber);

                    this.enemyBody.Position = new Vector2(randomX, randomY);
                    break;
                default:
                    this.enemyBody.Position = new Vector2(70, 70);
                    break;
                }
            
            simulator.Add(this.enemyBody);
            simulator.Add(this.enemyGeom);
        }

        public void preInit()
        {
            this.enemyBody.Position = new Vector2(0, -100);
        }

        public void setPosition(float x, float y)
        {
            this.enemyBody.Position = new Vector2(x, y);
        }

        public float getPositionX()
        {
            return this.enemyBody.Position.X;
        }

        public float getPositionY()
        {
            return this.enemyBody.Position.Y;
        }

        public Vector2 getGeomPosition()
        {
            return this.enemyGeom.Position;
        }

        public float getGeomRotation()
        {
            return this.enemyGeom.Rotation;
        }

        public Texture2D getTexture()
        {
            return this.enemyTexture;
        }

        public Vector2 getBodyOrigin()
        {
            return enemyOrigin;
        }

        public void Move(float x, float y)
        {
            this.enemyBody.Position += new Vector2(x, y);
        }

        public void setRotation(float Angle)
        {
            this.enemyBody.Rotation = Angle;
        }

        // ##### RESET  #####

        public int getLife()
        {
            return life;
        }

        public void takeLife()
        {
            this.life = life - 1;
        }

        public void destroy()
        {
            this.isDead = true;
            this.enemyBody.ResetDynamics();
            this.setPosition(-100, 0);

            this.enemyGeom.CollidesWith = CollisionCategory.None;
        }

        public bool isDestroyed()
        {
            return isDead;
        }

        public void setDeath(bool death)
        {
            this.isDead = death;
            if( death == true)
                life = 5;
        }

        public bool checkDestroyed()
        {
            return checkedDestroyed;
        }

        public void justCheckDestroyed(bool wasIt)
        {
            checkedDestroyed = wasIt;
        }

        public void reset(PhysicsSimulator simulator)
        {
            simulator.Remove(this.enemyBody);
            simulator.Remove(this.enemyGeom);
            if (type == "Tornado")
                life = 5;
            if (type == "Scouter")
                life = 1;
            if (type == "Bouncer")
                life = 1;
            if (type == "Bouncer2")
                life = 1;
            if (type == "Boss")
                life = 1;
            if (type == "BossFire")
                life = 1;
            this.preInit();
            this.setDeath(false);
            this.justCheckDestroyed(false);
            this.enemyGeom.CollidesWith = CollisionCategory.All & ~CollisionCategory.Cat1 & ~CollisionCategory.Cat2 & ~CollisionCategory.Cat3 & ~CollisionCategory.Cat4 & ~CollisionCategory.Cat5 & ~CollisionCategory.Cat7;
        }

        // ##### IA #####

        public string getType()
        {
            return this.type;
        }

        public Player ChoosePlayer(Player getPlayer1, Player getPlayer2, Player getPlayer3, Player getPlayer4, int numPlayers)
        {
            bool finished = false;

            while (finished == false)
            {
                
                int rand = random.Next(1, numPlayers);
                switch (rand)
                {
                    case 1:
                        this.selPlayer = getPlayer1;
                        break;
                    case 2:
                        this.selPlayer = getPlayer2;
                        break;
                    case 3:
                        this.selPlayer = getPlayer3;
                        break;
                    case 4:
                        this.selPlayer = getPlayer4;
                        break;
                }
                if (!selPlayer.isDead() || !selPlayer.wasJustBumped())
                    finished = true;

            }


            return this.selPlayer;
        }


        public void followPlayer(Player chosenPlayer)
        {
            if (enemyBody.Position.X > chosenPlayer.getPositionX() && enemyBody.Position.Y > chosenPlayer.getPositionY())
            {

                float a = Vector2.Distance(chosenPlayer.getPosition(), new Vector2(enemyBody.Position.X, enemyBody.Position.Y));
                float b = enemyBody.Position.X - chosenPlayer.getPositionX();
                float c = enemyBody.Position.Y - chosenPlayer.getPositionY();

                float angle = (float)Math.Acos((a * a + b * b - c * c) / (2 * a * b));

                this.setRotation(angle);
            }
            else if (enemyBody.Position.X > chosenPlayer.getPositionX() && enemyBody.Position.Y < chosenPlayer.getPositionY())
            {
                float a = Vector2.Distance(chosenPlayer.getPosition(), new Vector2(enemyBody.Position.X, enemyBody.Position.Y));
                float b = enemyBody.Position.X - chosenPlayer.getPositionX();
                float c = chosenPlayer.getPositionY() - enemyBody.Position.Y;

                float angle = -(float)Math.Acos((a * a + b * b - c * c) / (2 * a * b));

                this.setRotation(angle);
            }
            else if (enemyBody.Position.X < chosenPlayer.getPositionX() && enemyBody.Position.Y > chosenPlayer.getPositionY())
            {
                float a = Vector2.Distance(chosenPlayer.getPosition(), new Vector2(enemyBody.Position.X, enemyBody.Position.Y));
                float b = chosenPlayer.getPositionX() - enemyBody.Position.X;
                float c = enemyBody.Position.Y - chosenPlayer.getPositionY();

                float angle = -(float)Math.Acos((a * a + b * b - c * c) / (2 * a * b));

                this.setRotation(angle + (float)Math.PI);
            }
            else if (enemyBody.Position.X < chosenPlayer.getPositionX() && enemyBody.Position.Y < chosenPlayer.getPositionY())
            {
                float a = Vector2.Distance(chosenPlayer.getPosition(), new Vector2(enemyBody.Position.X, enemyBody.Position.Y));
                float b = chosenPlayer.getPositionX() - enemyBody.Position.X;
                float c = enemyBody.Position.Y - chosenPlayer.getPositionY();

                float angle = (float)Math.Acos((a * a + b * b - c * c) / (2 * a * b));

                this.setRotation(angle + (float)Math.PI);
            }

            this.enemyBody.LinearVelocity = new Vector2((float)Math.Cos(enemyBody.Rotation) * -200, (float)Math.Sin(enemyBody.Rotation) * -200);
        }


        public void update(Player player1, Player player2, Player player3, Player player4) 
        {
            if(!isDead)
            {
                if(type == "Tornado")
                {
                if(enemyBody.Position.Y < 300)
                    enemyBody.ApplyForce(new Vector2(-100, 0));

                if(enemyBody.Position.X < 300)
                    enemyBody.ApplyForce(new Vector2( 0, 100));

                if (enemyBody.Position.Y > 500)
                    enemyBody.ApplyForce(new Vector2(100, 0));

                if (enemyBody.Position.X > 1000)
                    enemyBody.ApplyForce(new Vector2(0, -100));

                    enemyBody.ApplyTorque(1000);
                }
                if (type == "Scouter")
                {
                    if (this.playerNotChosen)
                    {
                        this.chosenPlayer = ChoosePlayer(player1, player2, player3, player4, 5);
                        this.playerNotChosen = false;
                    }

                    if (!chosenPlayer.wasJustBumped() && !chosenPlayer.isDead())
                    {
                        this.followPlayer(chosenPlayer);
                    }
                    else
                        this.playerNotChosen = true;
                }
                if (type == "Bouncer")
                {
                if (enemyBody.Position.X < 400)
                    enemyBody.ApplyImpulse(new Vector2(100, 0));

                if (enemyBody.Position.X > 900)
                    enemyBody.ApplyImpulse(new Vector2(-100, 0));
                }
                if (type == "Bouncer2")
                {
                    if (enemyBody.Position.Y < 200)
                        enemyBody.ApplyImpulse(new Vector2(0, 100));

                    if (enemyBody.Position.Y > 520)
                        enemyBody.ApplyImpulse(new Vector2(0, -100));
                }
            }

        }

    }
}
