﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using Microsoft.Xna.Framework.Net;
using Microsoft.Xna.Framework.Storage;

using FarseerGames.FarseerPhysics;
using FarseerGames.FarseerPhysics.Dynamics;
using FarseerGames.FarseerPhysics.Dynamics.Joints;
using FarseerGames.FarseerPhysics.Collisions;
using FarseerGames.FarseerPhysics.Factories;

namespace XeonProject
{
    class Border
    {
        Body borderBody;
        Geom borderGeom;

        public Border(bool isSide, float positionX, float positionY, PhysicsSimulator simulator)
        {
            

            if (!isSide)
            {
                borderBody = BodyFactory.Instance.CreateRectangleBody(1280, 8, 1);
                borderGeom = GeomFactory.Instance.CreateRectangleGeom(this.borderBody, 1280, 15);
            }
            else
            {
                borderBody = BodyFactory.Instance.CreateRectangleBody(8, 720, 1);
                borderGeom = GeomFactory.Instance.CreateRectangleGeom(this.borderBody, 15, 720);
            }


            this.borderBody.Position = new Vector2(positionX, positionY);

            borderGeom.RestitutionCoefficient = 0.0f;
            borderGeom.FrictionCoefficient = 0.0f;
            borderGeom.CollisionCategories = CollisionCategory.Cat30;
            borderGeom.CollidesWith = CollisionCategory.All & ~CollisionCategory.Cat6;

            borderBody.IsStatic = true;

            simulator.Add(this.borderBody);
            simulator.Add(this.borderGeom);
        }
    }
}
