﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace XeonProject
{
    class Screen
    {
        //Enregistre le controler du Joueur principal pour toute les screens

        

        public enum LogicalGamerIndex
        {
            One,
            Two,
            Three,
            Four
        }

        public static class LogicalGamer
        {
            private static readonly PlayerIndex[] playerIndices =
           {
              PlayerIndex.One,
              PlayerIndex.Two,
              PlayerIndex.Three,
              PlayerIndex.Four,
           };

            public static PlayerIndex GetPlayerIndex(LogicalGamerIndex index)
            {
                return playerIndices[(int)index];
            }

            public static void SetPlayerIndex(LogicalGamerIndex gamerIndex, PlayerIndex playerIndex)
            {
                playerIndices[(int)gamerIndex] = playerIndex;
            }
        }



        //The event associated with the Screen. This event is used to raise events
        //back in the main game class to notify the game that something has changed
        //or needs to be changed
        protected EventHandler ScreenEvent;
        public Screen(EventHandler theScreenEvent)
        {
            ScreenEvent = theScreenEvent;
        }

        //Update any information specific to the screen
        public virtual void Update(GameTime theTime)
        {

        }

        //Draw any objects specific to the screen
        public virtual void Draw(SpriteBatch theBatch)
        {

        }




    }
}
