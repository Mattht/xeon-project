﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using Microsoft.Xna.Framework.Net;
using Microsoft.Xna.Framework.Storage;

using FarseerGames.FarseerPhysics;
using FarseerGames.FarseerPhysics.Dynamics;
using FarseerGames.FarseerPhysics.Collisions;
using FarseerGames.FarseerPhysics.Factories;

namespace XeonProject
{
    class PlayScreen : Screen
    {

        PhysicsSimulator simulator = new PhysicsSimulator(new Vector2(0, 0));

        Rectangle window;

        Texture2D background;
        Texture2D mouseCursor;

        Vector2 mousePosition;


        Player player1;
        Player player2;
        Player player3;
        Player player4;

        SpriteFont ScoreFont;

        Spawn spawn;

        Border top;
        Border bottom;
        Border left;
        Border right;

        SoundEffect soundEffect;


        public PlayScreen(ContentManager theContent,GraphicsDeviceManager theGraphics, EventHandler theScreenEvent)
            : base(theScreenEvent)
        {
            window = new Rectangle(0, 0, theGraphics.GraphicsDevice.Viewport.Width, theGraphics.GraphicsDevice.Viewport.Height);

            background = theContent.Load<Texture2D>("Sprites\\BackgroundPlay");
            mouseCursor = theContent.Load<Texture2D>("Sprites\\AimCursor");
            ScoreFont = theContent.Load<SpriteFont>("Fonts\\ScoreFont");

            soundEffect = theContent.Load<SoundEffect>("Sounds\\Beep");

            top = new Border(false, 640, 28, simulator);
            bottom = new Border(false, 640, window.Height - 4, simulator);
            left = new Border(true, 4, 360, simulator);
            right = new Border(true, window.Width - 4, 360, simulator);


            spawn = new Spawn(theContent, simulator, player1, player2, player3, player4);

            player1 = new Player(theContent.Load<Texture2D>("Sprites\\Player"), theContent.Load<Texture2D>("Sprites\\Weapon"), theContent.Load<Texture2D>("Sprites\\Heart"));
            player2 = new Player(theContent.Load<Texture2D>("Sprites\\Player2"), theContent.Load<Texture2D>("Sprites\\Weapon2"), theContent.Load<Texture2D>("Sprites\\Heart2"));
            player3 = new Player(theContent.Load<Texture2D>("Sprites\\Player3"), theContent.Load<Texture2D>("Sprites\\Weapon3"), theContent.Load<Texture2D>("Sprites\\Heart3"));
            player4 = new Player(theContent.Load<Texture2D>("Sprites\\Player4"), theContent.Load<Texture2D>("Sprites\\Weapon4"), theContent.Load<Texture2D>("Sprites\\Heart4"));

            // Les Joueurs
            player1.init(590, 310, 180, 10, 140, 10, 1, simulator);
            player1.initMissiles(-10, -10, theContent, simulator);

                player2.init(690, 310, 380, 10, 340, 10, 2, simulator);
                player2.initMissiles(-10, -10, theContent, simulator);
                
                player3.init(590, 410, 580, 10, 540, 10, 3, simulator);
                player3.initMissiles(-10, -10, theContent, simulator);
                
                player4.init(690, 410, 780, 10, 740, 10, 4, simulator);
                player4.initMissiles(-10, -10, theContent, simulator);
                
            

        }

        //Update all of the elements that need updating in the Title Screen        
        public override void Update(GameTime theTime)
        {
            GamePadState gps1 = GamePad.GetState(LogicalGamer.GetPlayerIndex(LogicalGamerIndex.One));
            GamePadState gps2 = GamePad.GetState(LogicalGamer.GetPlayerIndex(LogicalGamerIndex.Two));
            GamePadState gps3 = GamePad.GetState(LogicalGamer.GetPlayerIndex(LogicalGamerIndex.Three));
            GamePadState gps4 = GamePad.GetState(LogicalGamer.GetPlayerIndex(LogicalGamerIndex.Four));

            const float speed = 5.5F;

            //Check to see if the Player one controller has pressed the "B" button, if so, then
            //call the screen event associated with this screen
            /*if (GamePad.GetState(PlayerOne).Buttons.B == ButtonState.Pressed || Keyboard.GetState().IsKeyDown(Keys.B) == true)
            {
                ScreenEvent.Invoke("Stop", new EventArgs());
            }*/


            // PlayerOne
            if (LogicalGamer.GetPlayerIndex(LogicalGamerIndex.One) == PlayerIndex.One)
            {
                player1.Move(gps1.ThumbSticks.Left.X * speed, -gps1.ThumbSticks.Left.Y * speed);
                player1.setPadWeaponRotation(gps1.ThumbSticks.Right.X, gps1.ThumbSticks.Right.Y);

                if (gps1.Buttons.RightShoulder == ButtonState.Pressed)
                    player1.Fire();
            }
            
            // PlayerTwo
            player2.Move(gps2.ThumbSticks.Left.X * speed, -gps2.ThumbSticks.Left.Y * speed);
            player2.setPadWeaponRotation(gps2.ThumbSticks.Right.X, gps2.ThumbSticks.Right.Y);

            if (gps2.Buttons.RightShoulder == ButtonState.Pressed)
                player2.Fire();

            player3.Move(gps3.ThumbSticks.Left.X * speed, -gps3.ThumbSticks.Left.Y * speed);
            player3.setPadWeaponRotation(gps3.ThumbSticks.Right.X, gps3.ThumbSticks.Right.Y);

            if (gps3.Buttons.RightShoulder == ButtonState.Pressed)
                player3.Fire();

            player4.Move(gps4.ThumbSticks.Left.X * speed, -gps4.ThumbSticks.Left.Y * speed);
            player4.setPadWeaponRotation(gps4.ThumbSticks.Right.X, gps4.ThumbSticks.Right.Y);

            if (gps4.Buttons.RightShoulder == ButtonState.Pressed)
                player4.Fire();




#if !XBOX
            KeyboardState keyboardState = Keyboard.GetState();
            MouseState mouseState = Mouse.GetState();



            // Control Player I
            if (LogicalGamer.GetPlayerIndex(LogicalGamerIndex.One) != PlayerIndex.One)
            {
                if (keyboardState.IsKeyDown(Keys.S)) { player1.Move(0, speed); }
                if (keyboardState.IsKeyDown(Keys.Q)) { player1.Move(-speed, 0); }
                if (keyboardState.IsKeyDown(Keys.Z)) { player1.Move(0, -speed); }
                if (keyboardState.IsKeyDown(Keys.D)) { player1.Move(speed, 0); }
            }

            // Control Player II
            /*if (keyboardState.IsKeyDown(Keys.Right) && keyboardState.IsKeyUp(Keys.Down) && keyboardState.IsKeyUp(Keys.Left) && keyboardState.IsKeyUp(Keys.Up) && player2.canFire()) { player2.setWeaponRotation(0F); player2.Fire(); }
            if (keyboardState.IsKeyUp(Keys.Right) && keyboardState.IsKeyDown(Keys.Down) && keyboardState.IsKeyUp(Keys.Left) && keyboardState.IsKeyUp(Keys.Up) && player2.canFire()) { player2.setWeaponRotation(1.5707F); player2.Fire(); }
            if (keyboardState.IsKeyUp(Keys.Right) && keyboardState.IsKeyUp(Keys.Down) && keyboardState.IsKeyDown(Keys.Left) && keyboardState.IsKeyUp(Keys.Up) && player2.canFire()) { player2.setWeaponRotation(3.1415F); player2.Fire(); }
            if (keyboardState.IsKeyUp(Keys.Right) && keyboardState.IsKeyUp(Keys.Down) && keyboardState.IsKeyUp(Keys.Left) && keyboardState.IsKeyDown(Keys.Up) && player2.canFire()) { player2.setWeaponRotation(4.7123F); player2.Fire(); }

            if (keyboardState.IsKeyDown(Keys.Down) && keyboardState.IsKeyDown(Keys.Right) && player2.canFire()) { player2.setWeaponRotation(0.7853F); player2.Fire(); }
            if (keyboardState.IsKeyDown(Keys.Down) && keyboardState.IsKeyDown(Keys.Left) && player2.canFire()) { player2.setWeaponRotation(2.3561F); player2.Fire(); }
            if (keyboardState.IsKeyDown(Keys.Up) && keyboardState.IsKeyDown(Keys.Left) && player2.canFire()) { player2.setWeaponRotation(3.9269F); player2.Fire(); }
            if (keyboardState.IsKeyDown(Keys.Up) && keyboardState.IsKeyDown(Keys.Right) && player2.canFire()) { player2.setWeaponRotation(5.4977F); player2.Fire(); }

            if (keyboardState.IsKeyDown(Keys.L)) { player2.Move(0, speed);}
            if (keyboardState.IsKeyDown(Keys.K)) { player2.Move(-speed, 0);}
            if (keyboardState.IsKeyDown(Keys.O)) { player2.Move(0, -speed);}
            if (keyboardState.IsKeyDown(Keys.M)) { player2.Move(speed, 0);}*/


            // Control Mouse (Joueur I)

            if (LogicalGamer.GetPlayerIndex(LogicalGamerIndex.One) != PlayerIndex.One)
            {
                mousePosition = new Vector2(mouseState.X - 25, mouseState.Y - 25);

                if (mouseState.X > player1.getPositionX() && mouseState.Y > player1.getPositionY())
                {

                    float a = Vector2.Distance(player1.getPosition(), new Vector2(mouseState.X, mouseState.Y));
                    float b = mouseState.X - player1.getPositionX();
                    float c = mouseState.Y - player1.getPositionY();

                    float angle = (float)Math.Acos((a * a + b * b - c * c) / (2 * a * b));

                    player1.setWeaponRotation(angle);
                }
                else if (mouseState.X > player1.getPositionX() && mouseState.Y < player1.getPositionY())
                {
                    float a = Vector2.Distance(player1.getPosition(), new Vector2(mouseState.X, mouseState.Y));
                    float b = mouseState.X - player1.getPositionX();
                    float c = player1.getPositionY() - mouseState.Y;

                    float angle = -(float)Math.Acos((a * a + b * b - c * c) / (2 * a * b));

                    player1.setWeaponRotation(angle);
                }
                else if (mouseState.X < player1.getPositionX() && mouseState.Y > player1.getPositionY())
                {
                    float a = Vector2.Distance(player1.getPosition(), new Vector2(mouseState.X, mouseState.Y));
                    float b = player1.getPositionX() - mouseState.X;
                    float c = mouseState.Y - player1.getPositionY();

                    float angle = -(float)Math.Acos((a * a + b * b - c * c) / (2 * a * b));

                    player1.setWeaponRotation(angle + (float)Math.PI);
                }
                else if (mouseState.X < player1.getPositionX() && mouseState.Y < player1.getPositionY())
                {
                    float a = Vector2.Distance(player1.getPosition(), new Vector2(mouseState.X, mouseState.Y));
                    float b = player1.getPositionX() - mouseState.X;
                    float c = mouseState.Y - player1.getPositionY();

                    float angle = (float)Math.Acos((a * a + b * b - c * c) / (2 * a * b));

                    player1.setWeaponRotation(angle + (float)Math.PI);
                }

                if (mouseState.LeftButton == ButtonState.Pressed && player1.canFire())
                {
                    player1.Fire();
                    //soundEffect.Play();
                }
            }

#endif


            //score();

            spawn.update(player1, player2, player3, player4, theTime, simulator);

            player1.Score();
            player1.Respawn((float)theTime.ElapsedGameTime.TotalSeconds);

            
                player2.Score();
                player2.Respawn((float)theTime.ElapsedGameTime.TotalSeconds);
            

            
                player3.Score();
                player3.Respawn((float)theTime.ElapsedGameTime.TotalSeconds);
            

           
                player4.Score();
                player4.Respawn((float)theTime.ElapsedGameTime.TotalSeconds);
            

            

            
            


            simulator.Update(theTime.ElapsedGameTime.Milliseconds * .001f);
            base.Update(theTime);
        }

        

        //Draw all of the elements that make up the Title Screen
        public override void Draw(SpriteBatch theBatch)
        {

            theBatch.Draw(background, window, Color.White);

            

            theBatch.Draw(player1.getTexture(), player1.getGeomPosition(), null, Color.White, player1.getGeomRotation(), player1.getBodyOrigin(), 1, SpriteEffects.None, 1);
            theBatch.Draw(player2.getTexture(), player2.getGeomPosition(), null, Color.White, player2.getGeomRotation(), player2.getBodyOrigin(), 1, SpriteEffects.None, 1);
            theBatch.Draw(player3.getTexture(), player3.getGeomPosition(), null, Color.White, player3.getGeomRotation(), player3.getBodyOrigin(), 1, SpriteEffects.None, 1);
            theBatch.Draw(player4.getTexture(), player4.getGeomPosition(), null, Color.White, player4.getGeomRotation(), player4.getBodyOrigin(), 1, SpriteEffects.None, 1);


            theBatch.Draw(player1.getWeaponTexture(), player1.getWeaponGeomPosition(), null, Color.White, player1.getWeaponRotation(), player1.getWeaponOrigin(), 1, SpriteEffects.None, 1);
            theBatch.Draw(player2.getWeaponTexture(), player2.getWeaponGeomPosition(), null, Color.White, player2.getWeaponRotation(), player2.getWeaponOrigin(), 1, SpriteEffects.None, 1);
            theBatch.Draw(player3.getWeaponTexture(), player3.getWeaponGeomPosition(), null, Color.White, player3.getWeaponRotation(), player3.getWeaponOrigin(), 1, SpriteEffects.None, 1);
            theBatch.Draw(player4.getWeaponTexture(), player4.getWeaponGeomPosition(), null, Color.White, player4.getWeaponRotation(), player4.getWeaponOrigin(), 1, SpriteEffects.None, 1);


            for (int i = 0; i < Player.getMaxMissiles(); i++)
            {
                theBatch.Draw(player1.mainMissile[i].getMissileTexture(), player1.mainMissile[i].getMissilePosition(), null, Color.White, player1.mainMissile[i].getMissileRotation(), player1.mainMissile[i].getMissileOrigin(), 1, SpriteEffects.None, 1);
                
                theBatch.Draw(player2.mainMissile[i].getMissileTexture(), player2.mainMissile[i].getMissilePosition(), null, Color.White, player2.mainMissile[i].getMissileRotation(), player2.mainMissile[i].getMissileOrigin(), 1, SpriteEffects.None, 1);
                
                theBatch.Draw(player3.mainMissile[i].getMissileTexture(), player3.mainMissile[i].getMissilePosition(), null, Color.White, player3.mainMissile[i].getMissileRotation(), player3.mainMissile[i].getMissileOrigin(), 1, SpriteEffects.None, 1);
                
                theBatch.Draw(player4.mainMissile[i].getMissileTexture(), player4.mainMissile[i].getMissilePosition(), null, Color.White, player4.mainMissile[i].getMissileRotation(), player4.mainMissile[i].getMissileOrigin(), 1, SpriteEffects.None, 1);
            }
            
            
            theBatch.DrawString(ScoreFont, player1.getScore(), player1.getScorePosition(), Color.White);
            theBatch.Draw(player1.getHearTexture(), new Vector2(120, 20), null, Color.White, 0, new Vector2(12, 12), 1, SpriteEffects.None, 1);
            theBatch.DrawString(ScoreFont, player1.getLife(), player1.getLifePosition(), Color.White);

            
                theBatch.DrawString(ScoreFont, player2.getScore(), player2.getScorePosition(), Color.White);
                theBatch.Draw(player2.getHearTexture(), new Vector2(320, 20), null, Color.White, 0, new Vector2(12, 12), 1, SpriteEffects.None, 1);
                theBatch.DrawString(ScoreFont, player2.getLife(), player2.getLifePosition(), Color.White);
            

            
                theBatch.DrawString(ScoreFont, player3.getScore(), player3.getScorePosition(), Color.White);
                theBatch.Draw(player3.getHearTexture(), new Vector2(520, 20), null, Color.White, 0, new Vector2(12, 12), 1, SpriteEffects.None, 1);
                theBatch.DrawString(ScoreFont, player3.getLife(), player3.getLifePosition(), Color.White);
            

            
                theBatch.DrawString(ScoreFont, player4.getScore(), player4.getScorePosition(), Color.White);
                theBatch.Draw(player4.getHearTexture(), new Vector2(720, 20), null, Color.White, 0, new Vector2(12, 12), 1, SpriteEffects.None, 1);
                theBatch.DrawString(ScoreFont, player4.getLife(), player4.getLifePosition(), Color.White);
            


            spawn.draw(theBatch);

            if (LogicalGamer.GetPlayerIndex(LogicalGamerIndex.One) != PlayerIndex.One)
                theBatch.Draw(mouseCursor, mousePosition, Color.White);

            base.Draw(theBatch);
        }
    }
}
