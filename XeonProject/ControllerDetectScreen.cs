﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Input;



namespace XeonProject
{
    class ControllerDetectScreen : Screen
    {

        Texture2D mControllerDetectScreenBackground;
        KeyboardState previousKeyboardState;

        public ControllerDetectScreen(ContentManager theContent, EventHandler theScreenEvent)
            : base(theScreenEvent)
        {
            mControllerDetectScreenBackground = theContent.Load<Texture2D>("Sprites\\BackgroundController");
        }


        //Update all of the elements that need updating in the Controller Detect Screen
        public override void Update(GameTime theTime)
        {
            //Poll all the gamepads (and the keyboard) to check to see
            //which controller will be the player one controller. When the controlling
            //controller is detected, call the screen event associated with this screen

            for (int i = 0; i < 4; i++)
            {
                if (GamePad.GetState((PlayerIndex)i).Buttons.A == ButtonState.Pressed)
                {
                    LogicalGamer.SetPlayerIndex(LogicalGamerIndex.One, (PlayerIndex)i);
                    ScreenEvent.Invoke(this, new EventArgs());
                    
                }
            }



            if ((Keyboard.GetState().IsKeyDown(Keys.A) == true && previousKeyboardState.IsKeyUp(Keys.A)))
            {
                LogicalGamer.SetPlayerIndex(LogicalGamerIndex.One, (PlayerIndex)1);
                ScreenEvent.Invoke(this, new EventArgs());
            }
                   

            previousKeyboardState = Keyboard.GetState();

            base.Update(theTime);
        }


        //Draw all of the elements that make up the Controller Detect Screen
        public override void Draw(SpriteBatch theBatch)
        {
            theBatch.Draw(mControllerDetectScreenBackground, Vector2.Zero, Color.White);
            

            base.Draw(theBatch);
        }



    }
}
