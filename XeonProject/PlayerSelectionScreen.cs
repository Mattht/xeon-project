﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.GamerServices;



namespace XeonProject
{
    class PlayerSelectionScreen : Screen
    {

        Texture2D mControllerDetectScreenBackground;
        KeyboardState previousKeyboardState;

        // CONTROLLER DETECTORS
        Texture2D tController1;
        Texture2D tController2;
        Texture2D tController3;
        Texture2D tController4;

        Rectangle[] controllerRectangle;

        bool keyboardChosen;

        /*Rectangle rController1;
        Rectangle rController2;
        Rectangle rController3;
        Rectangle rController4;*/ 
        Vector2 tOrigin;

        public PlayerSelectionScreen(ContentManager theContent, EventHandler theScreenEvent)
            : base(theScreenEvent)
        {
            mControllerDetectScreenBackground = theContent.Load<Texture2D>("Sprites\\Background");

            tController1 = theContent.Load<Texture2D>("Sprites\\Controller1");
            tController2 = theContent.Load<Texture2D>("Sprites\\Controller2");
            tController3 = theContent.Load<Texture2D>("Sprites\\Controller3");
            tController4 = theContent.Load<Texture2D>("Sprites\\Controller4");

            tOrigin = new Vector2(this.tController1.Width / 4, this.tController1.Height / 2);

            controllerRectangle = new Rectangle[4];

            for (int i = 0; i < 4; i++)
                controllerRectangle[i] = new Rectangle(0, 0, 500, 250);


            /*rController1 = new Rectangle(0, 0, 500, 250);
            rController2 = new Rectangle(0, 0, 500, 250);
            rController3 = new Rectangle(0, 0, 500, 250);
            rController4 = new Rectangle(0, 0, 500, 250);*/

        }


        //Update all of the elements that need updating in the Controller Detect Screen
        public override void Update(GameTime theTime)
        {
            //Poll all the gamepads (and the keyboard) to check to see
            //which controller will be the player one controller. When the controlling
            //controller is detected, call the screen event associated with this screen

            if (LogicalGamer.GetPlayerIndex(LogicalGamerIndex.One) == PlayerIndex.One)
                controllerRectangle[0] = new Rectangle(500, 0, 1000, 250);

            if (Keyboard.GetState().IsKeyDown(Keys.A) == true && previousKeyboardState.IsKeyUp(Keys.A))
            {
                controllerRectangle[0] = new Rectangle(500, 0, 1000, 250);
                keyboardChosen = true;
            }

            if (keyboardChosen)
            {
                for (int i = 0; i < 4; i++) // i = num controller
                {
                    if (GamePad.GetState((PlayerIndex)i).Buttons.A == ButtonState.Pressed)
                    {
                        
                        LogicalGamer.SetPlayerIndex(LogicalGamerIndex.Two, (PlayerIndex)i);
                        controllerRectangle[1] = new Rectangle(500, 0, 1000, 250);
                        // move on to main menu
                    }
                }
            }
            else
            {
                for (int i = 0; i < 4; i++)
                {
                    if (GamePad.GetState((PlayerIndex)i).Buttons.A == ButtonState.Pressed)
                    {
                        LogicalGamer.SetPlayerIndex(LogicalGamerIndex.One, (PlayerIndex)i);
                        controllerRectangle[i] = new Rectangle(500, 0, 1000, 250);
                    }
                }
            }

            if ((Keyboard.GetState().IsKeyDown(Keys.Enter) == true && previousKeyboardState.IsKeyUp(Keys.Enter)) || GamePad.GetState((PlayerIndex)0).Buttons.Start == ButtonState.Pressed)
                ScreenEvent.Invoke("Start", new EventArgs());
                        
            
            previousKeyboardState = Keyboard.GetState();

            base.Update(theTime);
        }


        //Draw all of the elements that make up the Controller Detect Screen
        public override void Draw(SpriteBatch theBatch)
        {
            theBatch.Draw(mControllerDetectScreenBackground, Vector2.Zero, Color.White);

            theBatch.Draw(tController1, new Vector2(360, 200), controllerRectangle[0], Color.White, 0, tOrigin, 1, SpriteEffects.None, 1);
            theBatch.Draw(tController2, new Vector2(920, 200), controllerRectangle[1], Color.White, 0, tOrigin, 1, SpriteEffects.None, 1);
            theBatch.Draw(tController3, new Vector2(360, 520), controllerRectangle[2], Color.White, 0, tOrigin, 1, SpriteEffects.None, 1);
            theBatch.Draw(tController4, new Vector2(920, 520), controllerRectangle[3], Color.White, 0, tOrigin, 1, SpriteEffects.None, 1);
            // X: 320, 960  Y: 180, 540

            base.Draw(theBatch);
        }



    }
}
