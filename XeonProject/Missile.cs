﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using Microsoft.Xna.Framework.Net;
using Microsoft.Xna.Framework.Storage;

using FarseerGames.FarseerPhysics;
using FarseerGames.FarseerPhysics.Dynamics;
using FarseerGames.FarseerPhysics.Dynamics.Joints;
using FarseerGames.FarseerPhysics.Collisions;
using FarseerGames.FarseerPhysics.Factories;

namespace XeonProject
{
    class Missile
    {
        Body missileBody;
        Geom missileGeom;
        Texture2D missileTexture;
        Vector2 missileOrigin;

        public int score = 0;

        public Missile(Texture2D loadedTexture)
        {
            missileTexture = loadedTexture;
            missileOrigin = new Vector2(this.missileTexture.Width / 2, this.missileTexture.Height / 2);

            missileBody = BodyFactory.Instance.CreateRectangleBody(this.missileTexture.Height, this.missileTexture.Width, 1);
            missileBody.LinearDragCoefficient = 0.1F;
            missileBody.RotationalDragCoefficient = 0.1F;

            missileGeom = GeomFactory.Instance.CreateRectangleGeom(this.missileBody, 10, 21);
            missileGeom.RestitutionCoefficient = 0.0f;
            missileGeom.FrictionCoefficient = 0.0f;
            missileGeom.CollisionCategories = CollisionCategory.Cat6;
            missileGeom.CollidesWith = CollisionCategory.All & ~CollisionCategory.Cat1 & ~CollisionCategory.Cat2 & ~CollisionCategory.Cat3 & ~CollisionCategory.Cat4 & ~CollisionCategory.Cat5 & ~CollisionCategory.Cat6 & ~CollisionCategory.Cat7 & ~CollisionCategory.Cat30; // Player 1,2,3,4

            missileGeom.Tag = this;
            missileGeom.OnCollision += this.GoodAim;
        }


        // ##### INIT #####

        public void init(float startX, float startY, PhysicsSimulator simulator)
        {
            this.missileBody.Position = new Vector2(startX, startY);
            
            simulator.Add(this.missileBody);
            simulator.Add(this.missileGeom);
        }

        public Vector2 getMissilePosition()
        {
            return missileGeom.Position;
        }

        public float getMissileRotation()
        {
            return missileGeom.Rotation;
        }

        public Texture2D getMissileTexture()
        {
            return missileTexture;
        }

        public Vector2 getMissileOrigin()
        {
            return missileOrigin;
        }

        // ##### Reset #####

        /*public void checkOffScreen()
        {
            if((this.missileBody.Position.X < 0 && this.missileBody.Position.X > -50) || (this.missileBody.Position.X > 1280 && this.missileBody.Position.Y < 1330) || (this.missileBody.Position.Y < 0 && this.missileBody.Position.Y > -50) || (this.missileBody.Position.Y > 720 && this.missileBody.Position.Y < 770))
                this.resetMissile();
        }*/ 

        public void resetMissile()
        {
            this.missileBody.ResetDynamics();
            this.missileBody.Position = new Vector2(0, -200);
        }

        // ##### FIRE #####

        public void fireMissile(Body weaponBody)
        {
            this.missileBody.ResetDynamics();
            this.missileBody.Position = weaponBody.Position;
            this.missileBody.Rotation = weaponBody.Rotation + 1.65F;
            this.missileBody.LinearVelocity = new Vector2((float)Math.Cos(weaponBody.Rotation) * 1000, (float)Math.Sin(weaponBody.Rotation) * 1000);
        }

        // ##### COLLISION #####

        private bool GoodAim(Geom geom1, Geom geom2, ContactList contactList)
        {
            Missile missile = geom1.Tag as Missile;// on tente de voir si la geom1 ou la geom2 ont pour Tag un mec qu'on peut tuer
            Enemy enemy = geom2.Tag as Enemy;

            if (missile == null)
            {
                missile = geom2.Tag as Missile;
                enemy = geom1.Tag as Enemy;
            }

            if (missile != null && enemy != null)
            {

                this.resetMissile();

                if (enemy.getLife() <= 1)
                {
                    enemy.destroy();

                    if (enemy.getType() == "Tornado")
                    {
                        this.score = this.score + 10;
                    }
                    if (enemy.getType() == "Scouter")
                    {
                        this.score = this.score + 20;
                    }
                    if (enemy.getType() == "Bouncer")
                    {
                        this.score = this.score + 10;
                    }
                }
                else
                    enemy.takeLife();
            }

            return true;
        }

    }
}