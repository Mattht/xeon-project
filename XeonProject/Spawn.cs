﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using Microsoft.Xna.Framework.Net;
using Microsoft.Xna.Framework.Storage;

using FarseerGames.FarseerPhysics;
using FarseerGames.FarseerPhysics.Dynamics;
using FarseerGames.FarseerPhysics.Collisions;
using FarseerGames.FarseerPhysics.Factories;

namespace XeonProject
{
    class Spawn
    {

        // StartCounter

        Texture2D counterTexture;
        Rectangle viewNumber;

        int show = 0;


        //Wave wave1;
        /*Wave wave2;
        Wave wave3;
        Wave wave4;
        Wave wave5;
        Wave wave6;
        Wave wave7;
        Wave wave8;
        Wave wave9;
        Wave wave10;
        Wave wave11;*/ 

        Wave[] waveTornado;
        Wave[] waveScouter;
        Wave[] waveBouncer;

        int j;
        int b;

        SpriteFont ScoreFont;

        float elapsedTime;

        bool gameOver;

        // Start
        float countDown = 1000;
        float startTime = 5000;
        float endTime = 5000;


        public Spawn(ContentManager theContent, PhysicsSimulator simulator, Player player1, Player player2, Player player3, Player player4)
        {
            // CountDown
            counterTexture = theContent.Load<Texture2D>("Sprites\\Startup");
            viewNumber = new Rectangle(800, 0, 200, 200);
            ScoreFont = theContent.Load<SpriteFont>("Fonts\\ScoreFont");
            // ---------

            gameOver = false;

            waveTornado = new Wave[10];
            for (int i = 0; i < 10; i++)
            {
                waveTornado[i] = new Wave(theContent, simulator, 8, "Tornado");
            }

            j = 2;
            waveScouter = new Wave[10];
            for (int i = 0; i < 10; i++)
            {
                waveScouter[i] = new Wave(theContent, simulator, 15, "Scouter");
            }

            b = 6;
            waveBouncer = new Wave[10];
            for (int i = 0; i < 10; i++)
            {
                waveBouncer[i] = new Wave(theContent, simulator, 15, "Bouncer");
            }

            //wave1 = new Wave(theContent, simulator, 8, "Tornado");
            /*wave2 = new Wave(theContent, simulator, 15, "Scouter");
            wave3 = new Wave(theContent, simulator, 15, "Scouter");
            wave4 = new Wave(theContent, simulator, 15, "Scouter");
            wave5 = new Wave(theContent, simulator, 15, "Scouter");
            wave6 = new Wave(theContent, simulator, 6, "Bouncer");
            wave7 = new Wave(theContent, simulator, 6, "Bouncer2");
            wave8 = new Wave(theContent, simulator, 15, "Scouter");
            wave9 = new Wave(theContent, simulator, 15, "Scouter");
            wave10 = new Wave(theContent, simulator, 15, "Scouter");
            wave11 = new Wave(theContent, simulator, 15, "Scouter");*/ 
            
        }


        public void update(Player player1, Player player2, Player player3, Player player4, GameTime theTime, PhysicsSimulator simulator)
        {
            elapsedTime = theTime.ElapsedGameTime.Milliseconds;

            // START
            if (elapsedTime < startTime + 1000)
            {
                if (elapsedTime >= countDown)
                {
                    //show++;
                    show = show + 200;

                    viewNumber = new Rectangle(800 - show, 0, 200, 200);
                    countDown = 1000;
                    if (show == 1000)
                    {
                        player1.setDeath(false);
                        player2.setDeath(false);
                        player3.setDeath(false);
                        player4.setDeath(false);
                    }
                }
                else
                    countDown -= elapsedTime;
            }

           


            //wave1.createWave(simulator, elapsedTime, 1000, 1, player1, player2, player3, player4);
            /*wave2.createWave(simulator, elapsedTime, 28000, 2, player1, player2, player3, player4);
            wave3.createWave(simulator, elapsedTime, 30000, 3, player1, player2, player3, player4);
            wave4.createWave(simulator, elapsedTime, 32000, 4, player1, player2, player3, player4);
            wave5.createWave(simulator, elapsedTime, 34000, 5, player1, player2, player3, player4);
            wave6.createWave(simulator, elapsedTime, 40000, 6, player1, player2, player3, player4);
            wave7.createWave(simulator, elapsedTime, 40000, 7, player1, player2, player3, player4);
            wave8.createWave(simulator, elapsedTime, 41000, 8, player1, player2, player3, player4);
            wave9.createWave(simulator, elapsedTime, 43000, 9, player1, player2, player3, player4);
            wave10.createWave(simulator, elapsedTime, 45000, 10, player1, player2, player3, player4);
            wave11.createWave(simulator, elapsedTime, 45000, 11, player1, player2, player3, player4);*/ 

            for (int i = 0; i < 10; i++)
            {
                waveTornado[i].createWave(simulator, elapsedTime, i * 16000, 1, player1, player2, player3, player4);
                
                j++;
                if (j > 11)
                    j = 2;
                waveScouter[i].createWave(simulator, elapsedTime, i * 6000, j, player1, player2, player3, player4);

                b++;
                if (b > 7)
                    b = 6;
                waveBouncer[i].createWave(simulator, elapsedTime, i * 10000, b, player1, player2, player3, player4);
            }


            if (player1.getIntLife() < 1 && player2.getIntLife() < 1 && player3.getIntLife() < 1 && player4.getIntLife() < 1)
                gameOver = true;


        }


        public void draw(SpriteBatch theBatch)
        {

            for (int i = 0; i < 10; i++)
            {
                waveTornado[i].draw(theBatch);
                waveScouter[i].draw(theBatch);
                waveBouncer[i].draw(theBatch);
            }

            //wave1.draw(theBatch);
            /*wave2.draw(theBatch);
            wave3.draw(theBatch);
            wave4.draw(theBatch);
            wave5.draw(theBatch);
            wave6.draw(theBatch);
            wave7.draw(theBatch);
            wave8.draw(theBatch);
            wave9.draw(theBatch);
            wave10.draw(theBatch);
            wave11.draw(theBatch);*/ 


            if (show < 1000)
                theBatch.Draw(counterTexture, new Vector2(640, 360), viewNumber, Color.White, 0, new Vector2(100, 100), 1, SpriteEffects.None, 1);

            if (gameOver == true)
                theBatch.DrawString(ScoreFont, "Game Over" , new Vector2( 1150/2, 720/2), Color.White);
            

        }


    }
}