﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using Microsoft.Xna.Framework.Net;
using Microsoft.Xna.Framework.Storage;

using FarseerGames.FarseerPhysics;
using FarseerGames.FarseerPhysics.Dynamics;
using FarseerGames.FarseerPhysics.Collisions;
using FarseerGames.FarseerPhysics.Factories;

namespace XeonProject
{
    class Wave
    {
        
        Enemy[] enemy;
        Texture2D eTexture;

        int eNumber;
        string eType;

        int eLeft;

        float startTime;

        bool firstCall;
        bool waveInit;

        PhysicsSimulator simulator;

        public Wave(ContentManager theContent, PhysicsSimulator getSimulator, int enemiesInWave, string enemiesType)
        {
            simulator = getSimulator;

            eType = enemiesType;
            eNumber = enemiesInWave;

            enemy = new Enemy[eNumber];

            firstCall = true;
            waveInit = false;

            eLeft = enemiesInWave; 

            if(eType == "Tornado")
                eTexture = theContent.Load<Texture2D>("Sprites\\Enemy");
            if(eType == "Scouter")
                eTexture = theContent.Load<Texture2D>("Sprites\\Scouter");
            if(eType == "Bouncer")
                eTexture = theContent.Load<Texture2D>("Sprites\\Bouncer");
            if (eType == "Bouncer2")
                eTexture = theContent.Load<Texture2D>("Sprites\\Bouncer");

            for (int i = 0; i < eNumber; i++)
            {
                enemy[i] = new Enemy(eTexture, eType);
                enemy[i].preInit();
            }
        }

        public int getLeft()
        {
            return this.eLeft;
        }

        public void resetWave(PhysicsSimulator simulator)
        {
            for (int i = 0; i < eNumber; i++)
            {
                enemy[i].reset(simulator);
            }
            firstCall = true;
            waveInit = false;
            eLeft = eNumber;
        }

        public void createWave(PhysicsSimulator simulator, float elapsedTime, float getStartTime, int waveNumber, Player gplayer1, Player gplayer2, Player gplayer3, Player gplayer4)
        {
            if (firstCall)
            {
                startTime = getStartTime;
                firstCall = false;
            }


            if (elapsedTime >= startTime)
            {
                for (int i = 0; i < eNumber; i++)
                {
                    if (waveInit == false)
                    {
                        enemy[i].init(waveNumber, i, simulator);
                    }
                    enemy[i].update(gplayer1, gplayer2, gplayer3, gplayer4);

                    /*if (enemy[i].isDestroyed() && !enemy[i].checkDestroyed())
                    {
                        eLeft--;
                        enemy[i].justCheckDestroyed(true);
                    }

                    if (this.getLeft() < 1)
                        resetWave(simulator);*/ 
                    
                }
                waveInit = true;
            }
            else
                startTime -= elapsedTime;


        }

        public void draw(SpriteBatch theBatch)
        {
            for (int i = 0; i < eNumber; i++)
            {
                theBatch.Draw(enemy[i].getTexture(), enemy[i].getGeomPosition(), null, Color.White, enemy[i].getGeomRotation(), enemy[i].getBodyOrigin(), 1, SpriteEffects.None, 1);
            }
        }

    }
}
