﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using Microsoft.Xna.Framework.Net;
using Microsoft.Xna.Framework.Storage;

using FarseerGames.FarseerPhysics;
using FarseerGames.FarseerPhysics.Dynamics;
using FarseerGames.FarseerPhysics.Dynamics.Joints;
using FarseerGames.FarseerPhysics.Collisions;
using FarseerGames.FarseerPhysics.Factories;

/*
 * Collision cat:
 * 
 * Cat1 = Player1;
 * Cat2 = Player2;
 * Cat3 = Player3;
 * Cat4 = Player4;
 * Cat5 = Weapon;
 * Cat6 = Missile;
 * Cat7 = Enemy;
 * Cat30 = Map (Border);
 * 
 */ 


namespace XeonProject
{
    class Player
    {
        Body playerBody;
        Geom playerGeom;
        Texture2D playerTexture;
        Vector2 playerOrigin;

        Body weaponBody;
        Geom weaponGeom;
        Texture2D weaponTexture;
        Vector2 weaponOrigin;

        Texture2D heartTexture;

        RevoluteJoint playerJoint;
        public Missile[] mainMissile;
        static int maxMissiles = 16;

        // Score
        int score;
        Vector2 scorePosition;
        int tempScore;

        // Life
        int life = 5;
        Vector2 lifePosition;
        bool dead;

        // Fire
        int fireTime;
        int numbMissile;
        bool fired;

        // Respawn
        bool justBumped;
        bool isAllowedToFire = true;
        bool canMove = true;
        float refreshTimer = 5;
        float time = 0;

        public Player(Texture2D loadedTexture, Texture2D loadedTextureWeapon, Texture2D loadedTextureHeart)
        {

            heartTexture = loadedTextureHeart;

            //Gestion du "vaisseau"
            playerTexture = loadedTexture;
            playerOrigin = new Vector2(this.playerTexture.Width / 2, this.playerTexture.Height / 2);

            playerBody = BodyFactory.Instance.CreateRectangleBody(this.playerTexture.Height, this.playerTexture.Width, 1);
            playerBody.LinearDragCoefficient = 1;
            playerBody.RotationalDragCoefficient = 1000;

            playerGeom = GeomFactory.Instance.CreateCircleGeom(this.playerBody, 22, 50);
            playerGeom.RestitutionCoefficient = 0.0f;
            playerGeom.FrictionCoefficient = 0.0f;
            playerGeom.Tag = this;

            //Gestion de l'arme
            weaponTexture = loadedTextureWeapon;
            weaponOrigin = new Vector2(this.weaponTexture.Width / 2, this.weaponTexture.Height / 2);

            weaponBody = BodyFactory.Instance.CreateRectangleBody(this.weaponTexture.Height, this.weaponTexture.Width, 1);
            weaponBody.LinearDragCoefficient = 0;
            weaponBody.RotationalDragCoefficient = 0;
            weaponBody.Mass = 0.01F;

            
            Vertices weaponVertices = new Vertices(); // Anti Clock for some reason:
            weaponVertices.Add(new Vector2(0, 0));

            weaponGeom = new Geom(this.weaponBody, weaponVertices, 11);
            weaponGeom.RestitutionCoefficient = 0.0f;
            weaponGeom.FrictionCoefficient = 0.0f;
            //weaponGeom.CollisionEnabled = false;
            weaponGeom.CollisionCategories = CollisionCategory.Cat5;
            playerGeom.CollidesWith = CollisionCategory.All & ~CollisionCategory.Cat5 & ~CollisionCategory.Cat6;
            weaponGeom.CollidesWith = CollisionCategory.All & ~CollisionCategory.Cat1 & ~CollisionCategory.Cat2 & ~CollisionCategory.Cat3 & ~CollisionCategory.Cat4 & ~CollisionCategory.Cat6 & ~CollisionCategory.Cat7;

            playerJoint = JointFactory.Instance.CreateRevoluteJoint(this.playerBody, this.weaponBody, new Vector2(0, 0));
            
            //Gestion des missiles
            mainMissile = new Missile[maxMissiles];

            playerGeom.OnCollision += this.Bump;

        }

        //  #####   INIT
        #region Init

        public void init(float startX, float startY, float scoreX, float scoreY, float lifeX, float lifeY, int collisionCatNum, PhysicsSimulator simulator)
        {
            // Basic
            this.setPosition(startX, startY);
            this.setScorePosition(scoreX, scoreY);
            this.setLifePosition(lifeX, lifeY);
            this.setCollisionCat(collisionCatNum);

            // Body
            simulator.Add(this.getBody());
            simulator.Add(this.getGeom());

            // Weapon
            simulator.Add(this.getWeaponBody());
            simulator.Add(this.getWeaponGeom());
            simulator.Add(this.playerJoint);

            this.setDeath(true);
        }

        public void initMissiles(float startX, float startY, ContentManager theContent, PhysicsSimulator simulator)
        {
            for (int i = 0; i < maxMissiles; i++)
            {
                mainMissile[i] = new Missile(theContent.Load<Texture2D>("Sprites\\Missile"));
                mainMissile[i].init(startX, startY, simulator);
            }
        }

        public Texture2D getTexture()
        {
            return this.playerTexture;
        }

        public void setPosition(float x, float y)
        {
            this.playerBody.Position = new Vector2(x, y);
            this.weaponBody.Position = new Vector2(x, y);
        }

        public float getPositionX()
        {
            return this.playerBody.Position.X;
        }

        public float getPositionY()
        {
            return this.playerBody.Position.Y;
        }

        public Vector2 getPosition()
        {
            return this.playerBody.Position;
        }

        public Vector2 getBodyOrigin()
        {
            return playerOrigin;
        }

        public Vector2 getWeaponOrigin()
        {
            return weaponOrigin;
        }

        public void Move(float x, float y)
        {
            if (canMove)
            {
                this.playerBody.Position += new Vector2(x, y);
                this.weaponBody.Position += new Vector2(x, y);
            }
        }

        public Body getBody()
        {
            return this.playerBody;
        }

        public Geom getGeom()
        {
            return this.playerGeom;
        }

        public Vector2 getGeomPosition()
        {
            return this.playerGeom.Position;
        }

        public float getGeomRotation()
        {
            return this.playerGeom.Rotation;
        }

        public void setCollisionCat(int i)
        {
            if(i == 1)
                this.playerGeom.CollisionCategories = CollisionCategory.Cat1;
            if (i == 2)
                this.playerGeom.CollisionCategories = CollisionCategory.Cat2;
            if (i == 3)
                this.playerGeom.CollisionCategories = CollisionCategory.Cat3;
            if (i == 4)
                this.playerGeom.CollisionCategories = CollisionCategory.Cat4;
        }
        #endregion

        //  #####   WEAPON
        #region Weapon

        public Body getWeaponBody()
        {
            return this.weaponBody;
        }

        public Geom getWeaponGeom()
        {
            return this.weaponGeom;
        }

        public Texture2D getWeaponTexture()
        {
            return this.weaponTexture;
        }

        public Vector2 getWeaponGeomPosition()
        {
           return this.weaponGeom.Position;
        }

        public void setWeaponRotation(float i)
        {
            this.weaponBody.Rotation = i;
        }

        public float getWeaponRotation()
        {
            return this.weaponGeom.Rotation;
        }

        public void setWeaponRotation(float stickX, float stickY)
        {
            if (stickX > 0 && stickY > 0)
            {
                float a = Vector2.Distance(new Vector2(0, 0), new Vector2(stickX, stickY));
                float b = stickX;
                float c = stickY;

                float angle = (float)Math.Acos((a * a + b * b - c * c) / (2 * a * b));

                this.setWeaponRotation(angle);
            }
            else if (stickX > 0 && stickY < 0)
            {
                float a = Vector2.Distance(new Vector2(0, 0), new Vector2(stickX, stickY));
                float b = stickX - 0;
                float c = 0 - stickY;

                float angle = -(float)Math.Acos((a * a + b * b - c * c) / (2 * a * b));

                this.setWeaponRotation(angle);
            }
            else if (stickX < 0 && stickY > 0)
            {
                float a = Vector2.Distance(new Vector2(0, 0), new Vector2(stickX, stickY));
                float b = 0 - stickX;
                float c = stickY - 0;

                float angle = -(float)Math.Acos((a * a + b * b - c * c) / (2 * a * b));

                this.setWeaponRotation(angle + (float)Math.PI);
            }
            else if (stickX < 0 && stickY < 0)
            {
                float a = Vector2.Distance(new Vector2(0, 0), new Vector2(stickX, stickY));
                float b = 0 - stickX;
                float c = stickY - 0;

                float angle = (float)Math.Acos((a * a + b * b - c * c) / (2 * a * b));

                this.setWeaponRotation(angle + (float)Math.PI);
            }
        }

        public void setPadWeaponRotation(float stickX, float stickY)
        {
            if (stickX > 0 && stickY < 0)
            {
                float a = Vector2.Distance(new Vector2(0, 0), new Vector2(stickX, stickY));
                float b = stickX;
                float c = stickY;

                float angle = (float)Math.Acos((a * a + b * b - c * c) / (2 * a * b));

                this.setWeaponRotation(angle);
            }
            else if (stickX > 0 && stickY > 0)
            {
                float a = Vector2.Distance(new Vector2(0, 0), new Vector2(stickX, stickY));
                float b = stickX - 0;
                float c = 0 - stickY;

                float angle = -(float)Math.Acos((a * a + b * b - c * c) / (2 * a * b));

                this.setWeaponRotation(angle);
            }
            else if (stickX < 0 && stickY < 0)
            {
                float a = Vector2.Distance(new Vector2(0, 0), new Vector2(stickX, stickY));
                float b = 0 - stickX;
                float c = stickY - 0;

                float angle = -(float)Math.Acos((a * a + b * b - c * c) / (2 * a * b));

                this.setWeaponRotation(angle + (float)Math.PI);
            }
            else if (stickX < 0 && stickY > 0)
            {
                float a = Vector2.Distance(new Vector2(0, 0), new Vector2(stickX, stickY));
                float b = 0 - stickX;
                float c = stickY - 0;

                float angle = (float)Math.Acos((a * a + b * b - c * c) / (2 * a * b));

                this.setWeaponRotation(angle + (float)Math.PI);
            }


        }

        #endregion

        //  #####   COLISION
        #region Colision

        public bool wasJustBumped()
        {
            return justBumped;
        }

        private bool Bump(Geom geom1, Geom geom2, ContactList contactList)
          {
            Player player = geom1.Tag as Player;// on tente de voir si la geom1 ou la geom2 ont pour Tag un mec qu'on peut tuer
            Enemy enemy = geom2.Tag as Enemy;

            if (player == null)
            {
                player = geom2.Tag as Player;
                enemy = geom1.Tag as Enemy;
            }

            if (player != null && enemy != null)
            {
                life--;
                if (life < 1)
                {
                    this.playerBody.Position = new Vector2(-100, -100);
                    this.setDeath(true);
                }
                else
                {
                    this.playerBody.Position = new Vector2(1280 / 2, 720 / 2);
                    playerGeom.CollidesWith = CollisionCategory.All & ~CollisionCategory.Cat1 & ~CollisionCategory.Cat2 & ~CollisionCategory.Cat3 & ~CollisionCategory.Cat4 & ~CollisionCategory.Cat5 & ~CollisionCategory.Cat6 & ~CollisionCategory.Cat7;
                    justBumped = true;
                }
                this.weaponBody.Position = this.playerBody.Position;
                if (enemy.getType() == "Bouncer")
                    enemy.destroy();
            }

            return true;
          }
        #endregion

        //  ######  LIFE
        #region Life

        public Texture2D getHearTexture()
        {
            return heartTexture;
        }

        public void setLifePosition(float x, float y)
        {
            lifePosition = new Vector2(x, y);
        }

        public Vector2 getLifePosition()
        {
            return lifePosition;
        }

        public string getLife()
        {
            return this.life.ToString();
        }

        public int getIntLife()
        {
            return this.life;
        }

        public void setDeath(bool setState)
        {
            dead = setState;
            if (dead == true)
            {
                isAllowedToFire = false;
                canMove = false;
            }
            else
            {
                isAllowedToFire = true;
                canMove = true;
            }
        }

        public bool isDead()
        {
            return dead;
        }

        public void Respawn(float theTime)
        {
            if (this.justBumped)
            {
                time += theTime;

                if (refreshTimer <= time)
                {      // RESET LE GEOM POUR COLLISION...
                    playerGeom.CollidesWith = CollisionCategory.All & ~CollisionCategory.Cat5 & ~CollisionCategory.Cat6;
                    justBumped = false;
                    refreshTimer = 5;
                    time -= 5;
                }
            }


        }

        #endregion

        //  ######  SCORE
        #region Score

        public void Score()
        {
            for (int i = 0; i < maxMissiles; i++)
            {
                if (i == 0)
                    tempScore = 0;
                tempScore = tempScore + this.mainMissile[i].score;
            }
            this.score = tempScore;
        }

        public string getScore()
        {
            return this.score.ToString();
        }

        public void setScorePosition(float x, float y)
        {
            scorePosition = new Vector2(x, y);
        }

        public Vector2 getScorePosition()
        {
            return this.scorePosition;
        }

        #endregion

        //  ######  FIRE
        #region Fire

        /*public void checkMissile()
        {
            for (int i = 0; i < maxMissiles; i++)
            {
                this.mainMissile[i].checkOffScreen();
            }
        }*/ 

        public static int getMaxMissiles()
        {
            return maxMissiles;
        }

        public bool canFire()
        {
            return isAllowedToFire;
        }

        public void changeFire(bool trueOrFalse)
        {
            isAllowedToFire = trueOrFalse;
        }

        public void Fire()
        {
            if (this.canFire())
            {
                fireTime += 1;
                if (!fired)
                {
                    this.mainMissile[numbMissile].fireMissile(weaponBody);
                    fired = true;
                }
                if (fireTime > 5)
                {
                    if (numbMissile < Player.maxMissiles - 1)
                    {
                        numbMissile += 1;
                    }
                    else
                    {
                        numbMissile = 0;
                    }
                    fired = false;
                    fireTime = 0;
                }
            }

        }
        #endregion

        

    }
}
