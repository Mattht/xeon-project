﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using Microsoft.Xna.Framework.Net;
using Microsoft.Xna.Framework.Storage;

namespace XeonProject
{
    class Menu
    {
        public Rectangle ShowRectangle;

        public Texture2D buttonTexture;
        public Texture2D hoverTexture;
        public Texture2D unhoverTexture;
        public Vector2 Position;
        public Vector2 Origin;
        public float buttonWidth;
        public float buttonHeight;

        public Vector2 UpLeft;
        public Vector2 UpRight;
        public Vector2 DownLeft;
        public Vector2 DownRight;

        public bool isFocused;


        public Menu(Texture2D loadedTexture, Texture2D loadedHoverTexture)
        {
            buttonTexture = loadedTexture;
            hoverTexture = loadedHoverTexture;
            unhoverTexture = loadedTexture;
            buttonWidth = 180;
            buttonHeight = 60;
            Origin = new Vector2(buttonWidth / 2, buttonHeight / 2);
        }

    }
}
