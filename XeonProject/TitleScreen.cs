﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Input;



namespace XeonProject
{
    class TitleScreen : Screen
    {
        //Background texture pour TitleScreen
        Texture2D mTitleScreenBackground;
        Texture2D mouseCursor;
        Menu[] mainMenu;

        Vector2 mousePosition;

        int nextButton;
        int menuFocus;
        int menuMaxButton;
        Vector2 positionNextButton;

        KeyboardState previousKeyboardState;
        GamePadState previousPadState;

        

        public TitleScreen(ContentManager theContent, EventHandler theScreenEvent)
            : base(theScreenEvent)
        {

            previousKeyboardState = Keyboard.GetState();

            mTitleScreenBackground = theContent.Load<Texture2D>("Sprites\\BackgroundMenu");
            mouseCursor = theContent.Load<Texture2D>("Sprites\\AimCursor");

            menuMaxButton = 3;
            nextButton = 0;
            menuFocus = 0;
            positionNextButton = new Vector2(0, 0);

            mainMenu = new Menu[5];

            for (int i = 0; i < menuMaxButton; i++)
            {
                mainMenu[i] = new Menu(theContent.Load<Texture2D>("Sprites\\Menu"), theContent.Load<Texture2D>("Sprites\\MenuHover"));
                if (i == 0)
                    mainMenu[i].isFocused = true;
                else
                    mainMenu[i].isFocused = false;
                mainMenu[i].Position = new Vector2(1280 / 2, 300) - mainMenu[i].Origin + positionNextButton;
                mainMenu[i].ShowRectangle = new Rectangle(0, 0 + nextButton, 180, 60);
                mainMenu[i].UpLeft = mainMenu[i].Position;
                mainMenu[i].UpRight = mainMenu[i].Position + new Vector2(mainMenu[i].buttonWidth, 0);
                mainMenu[i].DownLeft = mainMenu[i].Position +  new Vector2(0, mainMenu[i].buttonHeight);
                mainMenu[i].DownRight = mainMenu[i].Position + new Vector2(mainMenu[i].buttonWidth, mainMenu[i].buttonHeight);
                positionNextButton += new Vector2(0, 80);
                nextButton += 60;
            }
        }


        //Update all of the elements that need updating in the Title Screen        
        public override void Update(GameTime theTime)
        {
            //Check to see if the Player one controller has pressed the "B" button, if so, then
            //call the screen event associated with this screen
            /*if (GamePad.GetState(PlayerOne).Buttons.B == ButtonState.Pressed || Keyboard.GetState().IsKeyDown(Keys.Enter) == true)
            {
                ScreenEvent.Invoke("Play", new EventArgs());
            }*/

            GamePadState gps1 = GamePad.GetState(LogicalGamer.GetPlayerIndex(LogicalGamerIndex.One));

            if ((gps1.DPad.Down == ButtonState.Pressed && previousPadState.DPad.Down == ButtonState.Released))
            {
                menuFocus++;
                if (menuFocus == menuMaxButton)
                    menuFocus = 0;

                mainMenu[menuFocus].isFocused = true;
                if (menuFocus != 0)
                    mainMenu[menuFocus - 1].isFocused = false;
                else
                    mainMenu[menuMaxButton - 1].isFocused = false;

            }
            else if (gps1.DPad.Up == ButtonState.Pressed && previousPadState.DPad.Up == ButtonState.Released)
            {
                menuFocus--;

                if (menuFocus == -1)
                    menuFocus = menuMaxButton - 1;

                mainMenu[menuFocus].isFocused = true;
                if (menuFocus != menuMaxButton - 1)
                    mainMenu[menuFocus + 1].isFocused = false;
                else
                    mainMenu[0].isFocused = false;

            }


                        
#if !XBOX
            KeyboardState keyboardState = Keyboard.GetState();
            
            MouseState mouseState = Mouse.GetState();
            mousePosition = new Vector2(mouseState.X , mouseState.Y);

            


            // MOUSE HOVER des boutons de menus
            if (mousePosition.X > mainMenu[0].UpLeft.X && mousePosition.Y > mainMenu[0].UpLeft.Y && mousePosition.X < mainMenu[0].UpRight.X && mousePosition.Y > mainMenu[0].UpRight.Y && mousePosition.X > mainMenu[0].DownLeft.X && mousePosition.Y < mainMenu[0].DownLeft.Y && mousePosition.X < mainMenu[0].DownRight.X && mousePosition.Y < mainMenu[0].DownRight.Y)
            {
                for (int i = 0; i < menuMaxButton; i++)
                {
                    if (i == 0)
                        mainMenu[0].isFocused = true;
                    else
                        mainMenu[i].isFocused = false;
                }
                menuFocus = 0;
            }
            else if (mousePosition.X > mainMenu[1].UpLeft.X && mousePosition.Y > mainMenu[1].UpLeft.Y && mousePosition.X < mainMenu[1].UpRight.X && mousePosition.Y > mainMenu[1].UpRight.Y && mousePosition.X > mainMenu[1].DownLeft.X && mousePosition.Y < mainMenu[1].DownLeft.Y && mousePosition.X < mainMenu[1].DownRight.X && mousePosition.Y < mainMenu[1].DownRight.Y)
            {
                for (int i = 0; i < menuMaxButton; i++)
                {
                    if (i == 1)
                        mainMenu[1].isFocused = true;
                    else
                        mainMenu[i].isFocused = false;
                }
                menuFocus = 1;
            }
            else if (mousePosition.X > mainMenu[2].UpLeft.X && mousePosition.Y > mainMenu[2].UpLeft.Y && mousePosition.X < mainMenu[2].UpRight.X && mousePosition.Y > mainMenu[2].UpRight.Y && mousePosition.X > mainMenu[2].DownLeft.X && mousePosition.Y < mainMenu[2].DownLeft.Y && mousePosition.X < mainMenu[2].DownRight.X && mousePosition.Y < mainMenu[2].DownRight.Y)
            {
                for (int i = 0; i < menuMaxButton; i++)
                {
                    if (i == 2)
                        mainMenu[2].isFocused = true;
                    else
                        mainMenu[i].isFocused = false;
                }
                menuFocus = 2;
            }

            // Gestion du clavier pour les boutons du menu
            if (keyboardState.IsKeyDown(Keys.Down) && previousKeyboardState.IsKeyUp(Keys.Down))
            {
                menuFocus ++;
                if (menuFocus == menuMaxButton)
                    menuFocus = 0;

                mainMenu[menuFocus].isFocused = true;
                if (menuFocus != 0)
                    mainMenu[menuFocus - 1].isFocused = false;
                else
                    mainMenu[menuMaxButton-1].isFocused = false;
                
            }
            else if (keyboardState.IsKeyDown(Keys.Up) && previousKeyboardState.IsKeyUp(Keys.Up))
            {
                menuFocus --;

                if (menuFocus == -1)
                    menuFocus = menuMaxButton - 1;

                mainMenu[menuFocus].isFocused = true;
                if (menuFocus != menuMaxButton - 1)
                    mainMenu[menuFocus + 1].isFocused = false;
                else
                    mainMenu[0].isFocused = false;
                
            }

            // SELECTED BUTTON a X temps
            if(mainMenu[0].isFocused == true)
            {
                for (int i = 0; i < menuMaxButton; i++)
                {
                    if (i == 0)
                        mainMenu[0].buttonTexture = mainMenu[0].hoverTexture;
                    else
                        mainMenu[i].buttonTexture = mainMenu[i].unhoverTexture;
                }
                if (mouseState.LeftButton == ButtonState.Pressed || keyboardState.IsKeyDown(Keys.Enter) || gps1.Buttons.A == ButtonState.Pressed)
                {
                    ScreenEvent.Invoke("Play", new EventArgs());
                }
            }
            else if (mainMenu[1].isFocused == true)
            {
                for (int i = 0; i < menuMaxButton; i++)
                {
                    if (i == 1)
                        mainMenu[1].buttonTexture = mainMenu[1].hoverTexture;
                    else
                        mainMenu[i].buttonTexture = mainMenu[i].unhoverTexture;
                }
            }
            else if (mainMenu[2].isFocused == true)
            {
                for (int i = 0; i < menuMaxButton; i++)
                {
                    if (i == 2)
                        mainMenu[2].buttonTexture = mainMenu[2].hoverTexture;
                    else
                        mainMenu[i].buttonTexture = mainMenu[i].unhoverTexture;
                }
                if (mouseState.LeftButton == ButtonState.Pressed || keyboardState.IsKeyDown(Keys.Enter) || gps1.Buttons.A == ButtonState.Pressed)
                {
                    ScreenEvent.Invoke("Exit", new EventArgs());
                }
            }


            previousKeyboardState = Keyboard.GetState();
            
#endif
            previousPadState = GamePad.GetState(PlayerIndex.One);

            base.Update(theTime);
        }


        // Fonction Draw pour le Title Screen
        public override void Draw(SpriteBatch theBatch)
        {
            theBatch.Draw(mTitleScreenBackground, Vector2.Zero, Color.White);

            for (int i = 0; i < menuMaxButton; i++)
            {
                theBatch.Draw(mainMenu[i].buttonTexture, mainMenu[i].Position, mainMenu[i].ShowRectangle, Color.White);
            }

            theBatch.Draw(mouseCursor, mousePosition - new Vector2(mouseCursor.Width / 2, mouseCursor.Height/2), Color.White);

            base.Draw(theBatch);
        }




    }
}
